from django.urls import path
from app.views import create, list, update, delete


urlpatterns = [
    path('', list, name='list'),
    path('create/', create, name='create'),
    path('update/<int:id>/', update, name="update"),
    path('delete/<int:id>/', delete, name="delete")
]